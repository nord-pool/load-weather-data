#!/usr/bin/python

import csv
import datetime
import os

class LoadElspotFromFile:
    def __init__(self, dbService):
        self.dbService = dbService

    # I bet datetime has this functionality
    def differentDay(self, currentTime,lastTime):
        return not (currentTime.year == lastTime.year and \
        currentTime.month == lastTime.month and \
        currentTime.day == lastTime.day)


    def run(self, fileFullName):
        #FIXME: this is shared with another project
        DATE_FORMAT = "%m/%d/%y %H:%M"
        with open(fileFullName) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            # getting rid of headers
            csvfile.next()
            csvfile.next()
            csvfile.next()
            lastTime = None
            currentTime = None
            prices = []
            for row in spamreader:
                utctime = ''.join(row[0])
                price = ''.join(row[1])
                currentTime = datetime.datetime.strptime(utctime, DATE_FORMAT)
                if lastTime and self.differentDay(currentTime,lastTime):
                    # not a perfect test that has all hours, but will do
                    if len(prices) == 24:
                        self.dbService.savePriceDay({'priceType': 'elspot', 'prices': prices}, currentTime, 'fi')
                        prices = []
                        prices.append({currentTime.strftime("%H"): price})
                    else:
                        print("Day had invalid hours: day: {}, hours: {}, number of hours: {}".format(currentTime, prices, len(prices)))
                        prices = []
                        prices.append({currentTime.strftime("%H"): price})

                    if currentTime.day % 20 == 0:
                        print(currentTime)
                else:
                    prices.append({currentTime.strftime("%H"): price})

                lastTime = currentTime

            # save last time
            if lastTime:
                if len(prices) != 0 and len(prices) != 24:
                    print("Day had invalid hours: day: {}, hours: {}, number of hours: {}".format(currentTime, prices, len(prices)))
                elif len(prices) == 24:
                    self.dbService.savePriceDay({'priceType': 'elspot', 'prices': prices}, currentTime, 'fi')
