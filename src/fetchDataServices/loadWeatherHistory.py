from Naked.toolshed.shell import execute_js, muterun_js

import random
import datetime
import ast
import pyorient
import sys

# node loadObservationForDate.js Helsinki 2015-01-01 /Users/timonikkila/AtomProjects/WeatherService/src/data hourlyObservation
# Helsinki 2015-01-01

class LoadDataObservations:
    def __init__(self, dbService, weatherService):
        self.dbService = dbService
        self.weatherService = weatherService

    def run(self, fromDate, toDate):
        city = "Kaisaniemi,Helsinki"
        weatherServicePath = "/Users/timonikkila/AtomProjects/WeatherService/src/"

        start_date = datetime.datetime.strptime(fromDate, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(toDate, '%Y-%m-%d')

        dates_between = self.weatherService.getDaysBetween(fromDate, toDate)
        current_date = start_date
        first_date = datetime.datetime(2003, 1, 1)

        while current_date.date() <= end_date.date():
            if first_date.date() <= current_date.date():
                dates_between.append(current_date.strftime('%Y-%m-%d'))
            current_date = current_date + datetime.timedelta(days=1)

        count = 0
        try:
            for runDate in dates_between:
                print("rundate: " + runDate)
                count += 1
                responseval = muterun_js(weatherServicePath + 'loadObservationForDate.js', city + " " + runDate)
                if responseval.exitcode == 0:
                    # this is stupid hack since json.loads does not work with current orientDb python driver.
                    # perhaps something to do with string unicode representation.
                    weatherData = responseval.stdout.replace("null", "None")
                    res = ast.literal_eval(weatherData)
                    record = {'records': res['timeValuePairs'], 'measure': 'temperature', 'recType': 'observation'}
                    loc = res['location']
                    groupName = loc['city']
                    location = {
                        'name': loc['name'],
                        'coordinates': loc['coordinates'],
                        'wmo': loc['wmo'],
                        'fmisid': loc['fmisid'],
                        'geoid': loc['geoid'],
                        'locationId': loc['id']
                    }

                    saveRecFormat = runDate.translate(None, "-")
                    self.dbService.saveNewRecord(record, saveRecFormat, location, groupName)
                else:
                    print(responseval.stderr)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
