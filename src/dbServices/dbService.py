from dbServices.orientDocumentModels import DocumentModels
import json


class DbService:
    def __init__(self, client):
        self.client = client

    def findOrCreateGroup(self, groupName):
        findGroup = "select * from {} where {} = '{}'".format(DocumentModels.locationGroup, 'name', groupName)
        existingGroup = self.client.command(findGroup)
        if len(existingGroup) == 0:
            insertGroup = "insert into {} set {} = '{}'".format(DocumentModels.locationGroup, 'name', groupName)
            newLocationGroup = self.client.command(insertGroup)
            return newLocationGroup[0]
        elif len(existingGroup) == 1:
            return existingGroup[0]
        else:
            raise Exception('more than one group with same name. Group name is ' + groupName)

    def findOrCreateLocation(self, location, groupName):
        findLocation = "select * from {} where {} = '{}'".format(DocumentModels.location, 'locationId', location['locationId'])
        existingLocation = self.client.command(findLocation)
        if len(existingLocation) == 0:
            groupDocument = self.findOrCreateGroup(groupName)
            locationInsert = { '@' + DocumentModels.location: location}
            locationDocument = self.client.record_create(-1, locationInsert)
            createContainEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.contain, groupDocument._rid, locationDocument._rid)
            self.client.command(createContainEdge)

            findPricingLocation = "select * from {} where {} = '{}'".format(DocumentModels.pricingArea, 'name', 'fi')
            prisingLocation = self.client.command(findPricingLocation)[0]
            createPricingLocationEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.pricingLocation, prisingLocation._rid, locationDocument._rid)
            self.client.command(createPricingLocationEdge)

            return locationDocument

        elif len(existingLocation) == 1:
            return existingLocation[0]
        else:
            raise Exception('more than one location with same locationId. locationId is ' + location['locationId'])

    def findOrCreateRecDate(self, recDate):
        findRecDate = "SELECT * FROM RecordDate where day = '{}'".format(recDate)
        recDateDocument = self.client.command(findRecDate)
        if len(recDateDocument) > 1:
            raise Exception("more than one RecordDate per actual day: " + recDate)
        elif(len(recDateDocument) == 0):
            addRecDay = "insert into {} set {} = '{}'".format(DocumentModels.recordDate, 'day', recDate)
            return self.client.command(addRecDay)[0]
        else:
            return recDateDocument[0]

    def createWeatherRecord(self, record, recDateDocument, locationDocument):
        recordInsert = {'@' + DocumentModels.weatherRecord: record}

        recordDocument = self.client.record_create(-1, recordInsert)
        createHappenEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.happen, recordDocument._rid, recDateDocument._rid)
        self.client.command(createHappenEdge)
        createRecordingEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.recording, locationDocument._rid, recordDocument._rid)
        return self.client.command(createRecordingEdge)

    def deleteOldWeatherRecord(self, record, recDate, locationDocument):
        # deletes edges automatically
        findWeatherRecIdRaw = \
        "select in as rid from ( traverse in_recording from (select * from ( " \
            "select expand(outV()) from (traverse in_happen from (SELECT * FROM RecordDate where day = '{}')) " \
        ") where recType = '{}' and measure= '{}')) where out= '{}'"
        findWeatherRecId = findWeatherRecIdRaw.format(recDate, record["recType"], record["measure"], locationDocument._rid)

        oldWeatherRecId = self.client.command(findWeatherRecId)
        deleteOldWeatherRec = "DELETE VERTEX FROM WeatherRecord WHERE @rid = {}".format(oldWeatherRecId[0].rid)
        return self.client.command(deleteOldWeatherRec)

    # recDate is string formatted as 20150101
    def saveNewRecord(self, record, recDate, location, groupName):
        locationDocument = self.findOrCreateLocation(location, groupName)
        recDateDocument = self.findOrCreateRecDate(recDate)

        rawQue = \
        "select count(*) from ( traverse in_recording from (select * from ( " \
            "select expand(outV()) from (traverse in_happen from (SELECT * FROM RecordDate where day = '{}')) " \
        ") where recType = '{}' and measure= '{}')) where out= '{}'"
        que = rawQue.format(recDate, record["recType"], record["measure"], locationDocument._rid)
        recordsFound = self.client.command(que)

        if recordsFound[0].count == 0:
            self.createWeatherRecord(record, recDateDocument, locationDocument)
        if recordsFound[0].count == 1:
            self.deleteOldWeatherRecord(record, recDate, locationDocument)
            self.createWeatherRecord(record, recDateDocument, locationDocument)
        if recordsFound[0].count > 1:
            raise Exception("more than one record for same type, same date, same location")


    #######################
    def findPricingArea(self, priceAreaName):
         findPricingArea = "select * from {} where {} = '{}'".format(DocumentModels.pricingArea, 'name', priceAreaName)
         return self.client.command(findPricingArea)[0]

    def createPrice(self, price, day, priceAreaName):
        priceInsert = {'@' + DocumentModels.price: price}
        priceDocument = self.client.record_create(-1, priceInsert)
        recordDateDocument = self.findOrCreateRecDate(day)
        pricingAreaDocument = self.findPricingArea(priceAreaName)

        createPricingDateEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.pricingDate, priceDocument._rid, recordDateDocument._rid)
        self.client.command(createPricingDateEdge)
        createDefinesEdge = "CREATE EDGE {} FROM {} TO {}".format(DocumentModels.defines, pricingAreaDocument._rid, priceDocument._rid)
        self.client.command(createDefinesEdge)
        return priceDocument

    def deletePrice(self, pricingAreaName, day):
        pricingAreaDocument = self.findPricingArea(pricingAreaName)
        findPriceToDelete = "select in as rid from (traverse in_{} from (select * from "\
            "(select expand(outV()) from (traverse in_{} from ( select * from {} where {} = '{}'))) " \
        "where {} = '{}')) where out = {}"\
        .format(DocumentModels.defines, DocumentModels.pricingDate, DocumentModels.recordDate, \
        'day', day, 'priceType', 'elspot', pricingAreaDocument._rid)
        priceRidToDelete = self.client.command(findPriceToDelete)
        if len(priceRidToDelete) > 1:
            raise Exception("More than one Price for same date and same PriceArea. Date: {}, Area: {}"\
            .format(day, pricingAreaName))
        elif len(priceRidToDelete) == 0:
            print("No Price for the given date and PriceArea. Date: {}, Area: {}. Nothing was done."\
            .format(day, pricingAreaName))
        else:
            deletePrice = "DELETE VERTEX FROM {} WHERE @rid = {}".format(DocumentModels.price, priceRidToDelete[0].rid)
            return self.client.command(deletePrice)

    def countOldPricing(self, day, pricingAreaName):
        countPricingAreaRaw = \
        "select count(*) from ( select expand(outV()) from (" \
        "traverse in_{} from (select * from "\
            "(select expand(outV()) from (traverse in_{} from ( select * from {} where {} = '{}'))) " \
        "where {} = '{}'))) where {} = '{}'"

        countPricingArea = countPricingAreaRaw.format(DocumentModels.defines, DocumentModels.pricingDate, DocumentModels.recordDate, \
        'day', day, 'priceType', 'elspot', 'name', pricingAreaName)

        return self.client.command(countPricingArea)[0].count

    def savePriceDay(self, price, priceDay, priceAreaName):
        day = priceDay.strftime('%Y%m%d')
        count = self.countOldPricing(day, priceAreaName)

        if count > 1:
            raise Exception("more than one Pricing found. Day: " + day)
        elif count == 1:
            self.deletePrice(priceAreaName, day)
            self.createPrice(price, day, priceAreaName)
        elif count == 0:
            self.createPrice(price, day, priceAreaName)
        else:
            raise Exception("something weird happend " + day)

    ########################################
# 'observation', 'temperature', recDate, 'Helsinki Kaisaniemi'
    def getWeatherRecords(self, recType, measure, recDate, locationName):
        # TODO: finding location can be combined with later query
        findLocation = "select * from {} where {} = '{}'".format(DocumentModels.location, 'name', locationName)

        locationDocument = self.client.command(findLocation)

        if len(locationDocument) > 1:
            # invalid exception. Make some fix
            raise Exception("More than one location assosiated with the same name")
        elif len(locationDocument) == 0:
            raise Exception("No location found by give name " + locationName)

        #findX = """select expand(findRecord) from ( select findRecord("20150101", "observation", "temperature"))"""
        #print(self.client.command(findX)[0])

        findWeatherRecRaw = \
        "select records from (select expand(in) from (traverse in_recording from (select * from ( " \
            "select expand(out) from (traverse in_happen from (SELECT * FROM RecordDate where day = '{}')) " \
        ") where recType = '{}' and measure= '{}')) where out= '{}')"
        findWeatherRec = findWeatherRecRaw.format(recDate, recType, measure, locationDocument[0]._rid)
        #print(findWeatherRec)
        weatherRecDoc = self.client.command(findWeatherRec)
        if len(weatherRecDoc) < 1:
            return None
        elif len(weatherRecDoc) > 1:
            raise Exception("more than one found by given parmaters {}, {}, {}, {}".format(recType, measure, recDate, locationName))
        else: return weatherRecDoc[0].records

    def getPrices(self, recDate, areaName, priceType):
        findPricesRaw = "SELECT expand(out('{}')) FROM ( " \
        "SELECT FROM (SELECT expand(in('{}')) FROM ( "\
        "SELECT FROM ( "\
        "SELECT expand(in('{}')) FROM ("\
        "SELECT FROM RecordDate WHERE day = '{}' "\
        ")"\
        ") WHERE priceType = '{}')"\
        ") WHERE name = '{}')"
        findPrices = findPricesRaw.format(DocumentModels.defines, DocumentModels.defines, DocumentModels.pricingDate, recDate, priceType, areaName)
        print(findPrices)
        return self.client.command(findPrices)[0].prices
