import importlib
from dbServices.testOrientDbConfig import OrientConfig
from dbServices.publicApi import PublicDbApi
import datetime

class TestData:
    def run(self):
        with PublicDbApi(OrientConfig) as dbService:
            recDate = "20150101"
            priceDay = datetime.datetime.strptime(recDate, '%Y%m%d')
            prices = {'priceType': 'elspot', 'prices': [{"hour": "01", "value":30.13}, {"hour":"02", "value":33.11}, {"hour":"03", "value":30.91},{ "hour":"04", "value":12.13}]}
            priceAreaName = 'fi'
            dbService.savePriceDay(prices, priceDay, priceAreaName)


            record = {'records': [{'value': 3.9, 'time': '2015-01-01T00:00:00.000Z'}, {'value': 3.1, 'time': '2015-01-01T01:00:00.000Z'},], \
             'measure': 'temperature', 'recType': 'observation'}
            location = {"locationId": "testinggeoid123123", 'name': 'Helsinki Kaisaniemi'}
            groupName = "Helsinki"
            dbService.saveNewRecord(record, recDate, location, groupName)
