from dbService import DbService
import pyorient

class PublicDbApi:
    def __init__(self, orientConfig):
        self.client = pyorient.OrientDB(orientConfig.url, orientConfig.port)
        session_id = self.client.connect(orientConfig.userName, orientConfig.password)
        self.client.db_open(orientConfig.dbName, orientConfig.roleAdmin, orientConfig.passwordAdmin)
        self.dbService = DbService(self.client)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.client.db_close()

    def saveNewRecord(self, record, recDate, location, groupName):
        return self.dbService.saveNewRecord(record, recDate, location, groupName)

    def savePriceDay(self, prices, priceDay, priceAreaName):
        return self.dbService.savePriceDay(prices, priceDay, priceAreaName)

    def getWeatherRecords(self, recType, measure, recDate, locationName):
        return self.dbService.getWeatherRecords(recType, measure, recDate, locationName)

    def getPrices(self, recDate, areaName, priceType):
        return self.dbService.getPrices(recDate, areaName, priceType)
