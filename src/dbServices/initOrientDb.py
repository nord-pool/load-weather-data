import pyorient
import importlib

class InitDb:
    def run(self, orientConfig, documentModels):
        client = pyorient.OrientDB(orientConfig.url, orientConfig.port)
        session_id = client.connect(orientConfig.userName, orientConfig.password)
        db_name = orientConfig.dbName
        if client.db_exists( db_name, pyorient.STORAGE_TYPE_PLOCAL):
           client.db_drop(db_name)
        client.db_create(db_name, pyorient.DB_TYPE_GRAPH, pyorient.STORAGE_TYPE_PLOCAL)
        client.db_open(db_name, orientConfig.roleAdmin, orientConfig.passwordAdmin)
        client.command( "create class " + documentModels.locationGroup + " extends V" )
        client.command( "create class " + documentModels.location + " extends V" )
        client.command( "create class " + documentModels.weatherRecord + " extends V" )
        client.command( "create class " + documentModels.recordDate + " extends V" )
        client.command( "create class " + documentModels.price + " extends V" )
        client.command( "create class " + documentModels.pricingArea + " extends V" )

        client.command( "create class " + documentModels.recording + " extends E" ) # location->recording->record
        client.command( "create class " + documentModels.happen + " extends E" ) # record->happens->date
        client.command( "create class " + documentModels.contain + " extends E" ) #group->contain->location
        client.command( "create class " + documentModels.pricingDate + " extends E" ) #Price->pricingDate->date
        client.command( "create class " + documentModels.pricingLocation + " extends E" ) #PricingArea->pricingLocation->location
        client.command( "create class " + documentModels.defines + " extends E" ) #PricingArea->defines->price

        addFiArea = "insert into {} set {} = '{}'".format(documentModels.pricingArea, 'name', 'fi')
        client.command(addFiArea)
