class DocumentModels:
    locationGroup = "LocationGroup"
    location = 'Location'
    weatherRecord = 'WeatherRecord'
    recordDate =  'RecordDate'
    price = "Price"
    pricingArea = "PricingArea"
    recording = 'recording'
    happen = 'happen'
    contain = 'contain'
    pricingDate = 'pricingDate'
    pricingLocation = 'pricingLocation'
    defines = 'defines'
