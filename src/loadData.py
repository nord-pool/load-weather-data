#!/usr/bin/python

import sys
from dbServices.publicApi import PublicDbApi
from dbServices.orientDbConfig import OrientConfig
from fetchDataServices.loadWeatherHistory import LoadDataObservations
from fetchDataServices.loadElspotPrices import LoadElspotFromFile
from services.generalService import WeatherService

if len(sys.argv) < 2:
    print("sript needs parameter")
elif sys.argv[1] == "weather":
    with PublicDbApi(OrientConfig) as dbService:
        LoadDataObservations(dbService, WeatherService).run("2015-01-06", "2015-06-06")

elif sys.argv[1] == "elprice":
    with PublicDbApi(OrientConfig) as dbService:
        LoadElspotFromFile(dbService).run('../data/predictionFile.csv')

else:
    print("given parameter does not match any command")
