from dbServices.orientDbConfig import OrientConfig as prodConf
from dbServices.testOrientDbConfig import OrientConfig as testConf
from dbServices.orientDocumentModels import DocumentModels
from dbServices.initOrientDb import InitDb
from dbServices.initTestData import TestData
import sys

if len(sys.argv) < 2:
    print("Settign up test or production?")
elif sys.argv[1] == "test":
    InitDb().run(testConf, DocumentModels)
    TestData().run()
elif sys.argv[1] == "prod":
    InitDb().run(prodConf, DocumentModels)
else:
    print("command {} is not recognizable").format(sys.argv[1])
